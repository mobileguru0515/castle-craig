
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    
	<div class="entry-content">
		<?php if ( is_active_sidebar( 'content-top' ) ) : ?>
			<div class="content_top_wrapper"><?php dynamic_sidebar('content-top'); ?></div>
		<?php endif; ?>
		<?php the_content(); ?> 
		<div class="clearfix"></div>
	</div><!-- .entry-content -->
	
	<footer class="entry-meta">
		<?php bootstrapBasicEditPostLink(); ?> 
	</footer>
</article><!-- #post-## -->