<?php
/**
 * Template for displaying pages
 * 
 * @package bootstrap-basic
 */

get_header();

/**
 * determine main column size from actived sidebar
 */

$main_column_size = bootstrapCCGetMainColumnSize();
?> 

<div class="container">
<div class="row">
<?php get_sidebar('left'); ?> 
	<div class="col-md-<?php echo $main_column_size; ?> content-area" id="main-column">
		<main id="main" class="test site-main" role="main">
			<?php 
			while (have_posts()) {
				the_post();

				get_template_part('partials/page/content', 'page');

				echo "\n\n";
				
				// If comments are open or we have at least one comment, load up the comment template
				if (comments_open() || '0' != get_comments_number()) {
					comments_template();
				}

				echo "\n\n";

			} //endwhile;
			?> 
		</main>
		<?php if ( is_active_sidebar( 'content-bottom' ) ) : ?>
			<div class="content_bottom_wrapper"><?php dynamic_sidebar('content-bottom'); ?></div>
		<?php endif; ?>
	</div>  
<?php 
    get_sidebar( 'right' );
?>
 <?php echo '</div></div>'; ?>
<?php get_footer(); ?> 